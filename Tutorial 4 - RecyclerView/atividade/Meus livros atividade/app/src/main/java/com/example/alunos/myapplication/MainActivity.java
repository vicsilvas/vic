package com.example.alunos.mainactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.example.alunos.myapplication.R;
import com.example.alunos.myapplication.add_livro;

import adapter.LivroAdapter;
import adapter.LivroViewHolder;
import model.Livro;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private ArrayList<Livro> listaLivros;
    RecyclerView rview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rview = findViewById(R.id.recylerView);
        listaLivros = new ArrayList<>();

        listaLivros.add(new Livro("O Senhor dos Anéis", "J. R. R. Tolkien",
                "Fantasia épica onde elfos, anões, hobbits e homens enfrentam os poderes do mal"));
        listaLivros.add(new Livro("Uma breve história do tempo", "Stephen W. Hawking",
                "uma introdução a alguns dos conceitos mais profundos da física"));
        listaLivros.add(new Livro("A espada da galáxia", "Marcelo Cassaro",
                "A premiada ficção científica onde alienígenas rivais resolvem suas disputas na Terra"));

        rview.setAdapter(new LivroAdapter(listaLivros, this));
        RecyclerView.LayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rview.setLayoutManager(layout);
    }

    public void onClick(View v){
        Intent intent = new Intent(MainActivity.this, add_livro.class);
        startActivityForResult(intent, 1);
    }

}
