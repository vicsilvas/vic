package com.example.alunos.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class add_livro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_livro);
    }

    public void verificaCampo(View v){

        EditText titulo = findViewById(R.id.insereTitulo);
        EditText autor = findViewById(R.id.insereAutor);
        EditText desc = findViewById(R.id.insereDesc);
        if(titulo.matches("")){
            Toast toast = Toast.makeText(getApplicationContext(), "Preencha o título!"),
        } else {
            return;
        }
        if(autor.matches("")){
            Toast toast = Toast.makeText(getApplicationContext(), "Preencha o autor!"),
        } else {
            return;
        }
        if(desc.matches("")){
            Toast toast = Toast.makeText(getApplicationContext(), "Preencha a descrição!"),
        } else {
            return;
        }
    }

    public void onClick(View v){
        Intent intent = new Intent(add_livro.this, com.example.alunos.mainactivity.MainActivity.class);
        startActivityForResult(intent, 1);
    }
}
