package com.example.alunos.jogoo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import java.text.BreakIterator;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    int num;
    Random gerador = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        num = gerador.nextInt(100);
        Log.d("num", Integer.toString(num));
    }

    public void jogo (View v) {
        EditText palpite = findViewById(R.id.editText);
        String valor = palpite.getText().toString();
        int valorConv = Integer.parseInt(valor);
        TextView msg = findViewById(R.id.msg);
        if(valorConv == num){
            msg.setText("Você acertou!");
            num = gerador.nextInt(100);
        } else if(valorConv < num){
            msg.setText("O número é menor");
        } else {
            msg.setText("O número é maior");
        }
    }
}
