package com.example.alunos.jogoo;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.content.Intent;
import android.view.View;
import android.content.Context;
import android.widget.EditText;
import android.widget.TextView;
import java.text.BreakIterator;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    int num;
    int tentativas;
    Random gerador = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        num = gerador.nextInt(1000);
        Log.d("num", Integer.toString(num));
    }

    public void jogo (View v) {
        EditText palpite = findViewById(R.id.editText);
        String valor = palpite.getText().toString();
        int valorConv = Integer.parseInt(valor);
        TextView msg = findViewById(R.id.msg);
        if(valorConv == num){
            msg.setText("Você acertou!");
            num = gerador.nextInt(1000);
            tentativas++;
        } else if(valorConv < num){
            msg.setText("O número é menor");
            tentativas++;
        } else {
            msg.setText("O número é maior");
            tentativas++;
        }
        Intent i = new Intent(MainActivity.this, Placar.class);
        Bundle b = new Bundle();
        b.putInt("tentativas",tentativas);
        i.putExtras(b);
        StartActivity(i);

        Context context = getActivity();
        SharedPreferences arquivo = context.getPreferences(Context.MODE_PRIVATE);
        int recorde = arquivo.getInt("recorde",0);
    }
}
