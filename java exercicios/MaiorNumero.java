import java.util.Scanner;

public class MaiorNumero{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int x, y;
        System.out.println("Digite um número x: ");
        x = input.nextInt();
        System.out.println("Digite um número y: ");
        y = input.nextInt();
        System.out.print("O maior número é: ");
        if(x>y){
            System.out.print(x);
        } else {
            System.out.print(y);
        }
    }
}
