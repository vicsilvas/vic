import java.util.Scanner;

public class Terceiro{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int soma=0, multiplicacao=1;
        System.out.println("Digite um número n: ");
        int n = input.nextInt();
        for(int i=1; i<n; i++){
            if(i%2!=0){
                soma += i;
            } else {
                multiplicacao *= i;
            }
        }
        System.out.print("A soma é ", soma);
        System.out.print(". A multiplicação é", multiplicacao);
    }
}
