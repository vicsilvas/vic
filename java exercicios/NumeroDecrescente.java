import java.util.Scanner;

public class NumeroDecrescente{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int n;
        System.out.println("Digite um número x: ");
        n = input.nextInt();
        while(n>=0){
            System.out.print(n + " ");
            n--;
        }               
    }
}
