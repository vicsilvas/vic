package com.example.alunos.androidtoolbar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import adapter.PessoaAdapter;

public class ViewItensFragment extends Fragment {
    private RecyclerView a;
    private MainActivity c;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_view, container, false);
        c = (MainActivity) getActivity();
        a = v.findViewById(R.id.Recycler);
        a.setAdapter(c.getAdapter());
        RecyclerView.LayoutManager d;
        d = new LinearLayoutManager(c.getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        a.setLayoutManager(d);
        return v;
    }
}
