package com.example.alunos.androidtoolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import model.Pessoa;
import static android.app.Activity.RESULT_OK;

public class AddItemsFragment extends Fragment {
    MainActivity atividade;
    EditText nome;
    EditText telefone;
    EditText email;
    Button salvar;
    ArrayList<Pessoa> lista;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_add_items_layout, container, false);
        nome = v.findViewById(R.id.editNome);
        telefone = v.findViewById(R.id.editTelefone);
        email = v.findViewById(R.id.editEmail);
        salvar = v.findViewById(R.id.enviarButton);
        atividade = (MainActivity) getActivity();
        lista = atividade.getLista();

        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if((nome.getText().toString().matches("")) || (telefone.getText().toString().matches("")) || (email.getText().toString().matches(""))){
                    Toast toast = Toast.makeText(atividade.getApplicationContext(), "Algum campo está vazio", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
            }
             Pessoa p = new Pessoa(nome.getText().toString(), telefone.getText().toString(), email.getText().toString());
             lista.add(p);
             Toast toast = Toast.makeText(atividade.getApplicationContext(), "Pessoinha adicionada com sucesso!", Toast.LENGTH_SHORT);
             toast.show();
        }

        }); {
            return v;
        }
    }
}
