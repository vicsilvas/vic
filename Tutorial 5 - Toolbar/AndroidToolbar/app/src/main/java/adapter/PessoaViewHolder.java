package adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.alunos.androidtoolbar.R;

public class PessoaViewHolder extends RecyclerView.ViewHolder {
    final TextView nome;
    final TextView telefone;
    final TextView email;

    public PessoaViewHolder(View itemView){
        super(itemView);
        nome = itemView.findViewById(R.id.txtNome);
        telefone = itemView.findViewById(R.id.txtTelefone);
        email = itemView.findViewById(R.id.txtEmail);
    }
}
