package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alunos.androidtoolbar.R;

import java.util.List;

import model.Pessoa;

public class PessoaAdapter extends RecyclerView.Adapter {
    private List<Pessoa> lista;
    private Context context;

    public PessoaAdapter(List<Pessoa> p, Context c){
        this.lista = p;
        this.context = c;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.fragment_view_items_layout,parent,false);
        return new PessoaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PessoaViewHolder theHolder = (PessoaViewHolder) holder;
        Pessoa pessoa = lista.get(position);
        theHolder.nome.setText(pessoa.getNome());
        theHolder.telefone.setText(pessoa.getTelefone());
        theHolder.email.setText(pessoa.getEmail());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }
}
