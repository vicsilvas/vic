package com.example.alunos.meuslivros;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.alunos.meuslivros.Livro;
import com.example.alunos.meuslivros.LivroDAO;
import com.example.alunos.meuslivros.LivroRoomDatabase;

import java.util.List;

public class LivroRepository {
    private LivroDAO livroDAO;
    private LiveData<List<Livro>> listaLivros;

    LivroRepository(Application application) {
        LivroRoomDatabase db = LivroRoomDatabase.getDatabase(application);
        livroDAO = db.livroDAO();
        listaLivros = livroDAO.getAllLivros();
    }
    LiveData<List<Livro>> getAllLivros(){
        return listaLivros;
    }
    public void insert (Livro livro){
        new insertAsyncTask(livroDAO).execute(livro);
    }

    public void update(Livro livro) {
        new updateAsyncTask(livroDAO).execute(livro);
    }

    public void delete(Livro livro) { new deleteAsyncTask(livroDAO).execute(livro);}

    private static class updateAsyncTask extends AsyncTask<Livro, Void, Void> {
        private LivroDAO mAsyncTaskDao;

        private updateAsyncTask(LivroDAO dao){
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(Livro... params){
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    private class deleteAsyncTask extends AsyncTask<Livro, Void, Void>{
        private LivroDAO mAsyncTaskDao;
        public deleteAsyncTask(LivroDAO dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground (final Livro... livros){
            mAsyncTaskDao.delete(livros[0]);
            return null;
        }
    }

    private class insertAsyncTask extends AsyncTask<Livro, Void, Void> {
        private LivroDAO mAsyncTaskDAO;
        insertAsyncTask(LivroDAO dao){
            mAsyncTaskDAO = dao;
        }
        @Override
        protected Void doInBackground (final Livro... params){
            mAsyncTaskDAO.insert(params[0]);
            return null;
        }
    }
}
