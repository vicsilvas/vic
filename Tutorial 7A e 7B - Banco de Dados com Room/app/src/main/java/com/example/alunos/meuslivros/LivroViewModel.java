package com.example.alunos.meuslivros;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.alunos.meuslivros.Livro;
import com.example.alunos.meuslivros.LivroRepository;

import java.util.List;

public class LivroViewModel extends AndroidViewModel {
    private LivroRepository repository;
    private LiveData<List<Livro>> listaLivros;

    public LivroViewModel (@NonNull Application application){
        super(application);
        repository = new LivroRepository(application);
        listaLivros = repository.getAllLivros();
    }
    public LiveData<List<Livro>> getAllLivros(){
        return repository.getAllLivros();
    }
    void insert (Livro livro) {
        repository.insert(livro);
    }

    public void update(Livro livro) {
        repository.update(livro);
    }

    public void delete(Livro livro) {
        repository.delete(livro);
    }
}
